"""Test cases for hash table."""
import hash_table
import unittest as ut


class TestHashTable(ut.TestCase):
    """Implement test cases for hash table."""

    def testIsEmpty(self):
        """Test isEmpty method functionality."""
        hashTable = hash_table.HashTable()
        self.assertTrue(hashTable.isEmpty('a'))

    def testInsertFunctionalityCase1(self):
        """
        Test insert functionality.

        Insert element into hash table
        """
        hashTable = hash_table.HashTable()
        hashTable.insert('archery')
        value = hashTable.table[0].seekFromHead(1)
        self.assertEqual(value, 'archery')

    def testInsertFunctionalityCase2(self):
        """
        Test insert functionality.

        When different elements are inserted at same index of hash table.
        """
        hashTable = hash_table.HashTable()
        hashTable.insert('badminton')
        hashTable.insert('basketball')
        value = hashTable.table[1].seekFromHead(2)
        self.assertEqual(value, 'basketball')

    def testSearchFunctionalityCase1(self):
        """
        Test search functionality.

        When hash table is empty.
        """
        hashTable = hash_table.HashTable()
        self.assertEqual(hashTable.search('badminton'), None)

    def testSearchFunctionalityCase2(self):
        """
        Test search functionality.

        When element present in hash table.
        """
        hashTable = hash_table.HashTable()
        hashTable.insert('badminton')
        hashTable.insert('basketball')
        hashTable.insert('archery')
        self.assertEqual(hashTable.search('basketball'), 'basketball')

    def testSearchFunctionalityCase3(self):
        """
        Test search functionality.

        When element not present in hash table.
        """
        hashTable = hash_table.HashTable()
        hashTable.insert('badminton')
        hashTable.insert('basketball')
        hashTable.insert('archery')
        self.assertEqual(hashTable.search('cricket'), None)
