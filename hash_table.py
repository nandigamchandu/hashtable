"""Implement hash table."""
import double_linked_list as dll


class HashTable:
    """Implement hash table."""

    def __init__(self, size=None):
        """Initialize size of hash table."""
        if not size:
            size = 26
        self.table = [None] * size

    def isEmpty(self, key):
        """Return true if hash table at given index is empty."""
        if not self.table[self.hashFunction(key)]:
            return True
        return False

    def hashFunction(self, key):
        """Implement hash function."""
        return ord(key[0]) - ord('a')

    def insertLinkedList(self, element):
        """To insert doubleLinkedList in empty location."""
        linkedList = dll.DoubleLinkedList()
        linkedList.append(element)
        return linkedList

    def insert(self, element):
        """To insert an element into hash table."""
        if self.isEmpty(element):
            head = self.insertLinkedList(element)
            self.table[self.hashFunction(element)] = head
        else:
            self.table[self.hashFunction(element)].append(element)

    def search(self, element):
        """Return element if element present in the hash table else None."""
        if self.table[self.hashFunction(element)]:
            return (self.table[self.hashFunction(element)]
                        .linearSearch(element))
        return None
