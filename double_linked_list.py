"""Implement Linked List."""


class Node:
    """Node implementation for doubly-linked list."""

    def __init__(self, v=None, next=None, prev=None):
        """Construct a node."""
        self.key = v
        self.next = next
        self.prev = prev


class DoubleLinkedList:
    """Implementation of doubly-linked list."""

    def __init__(self):
        """Construct a doubly-linked list."""
        self.head = None
        self.tail = None
        self.length = 0

    def isEmpty(self):
        """Return True of linked list is empty."""
        if self.length == 0:
            return True
        return False

    def prepend(self, key):
        """To insert element into list."""
        if self.isEmpty():
            self.insertFristNode(key)
        else:
            newCell = Node(key, self.head, None)
            self.head.prev = newCell
            self.head = newCell
            self.length += 1

    def append(self, key):
        """To insert element at the end of the double linkedList."""
        if self.isEmpty():
            self.insertFristNode(key)
        else:
            newCell = Node(key, None, self.tail)
            self.tail.next = newCell
            self.tail = newCell
            self.length += 1

    def insertFristNode(self, key):
        """To insert new key."""
        newCell = Node(key, self.head, None)
        self.head = newCell
        self.tail = newCell
        self.length = 1

    def simpleTraverse(self):
        """Traverse and print only node values."""
        node = self.head
        print('Head->', end='')
        while node is not None:
            print(f"{node.key}->", end='')
            node = node.next
        print('Tail')

    def remove(self, index):
        """To remove key at given index."""
        if index <= 0 or index > self.length:
            return -1
        node = self.head
        for i in range(1, index+1):
            if i == 1 and i == index:
                node.next.prev = None
                self.head = node.next
            elif i > 1 and i < self.length and i == index:
                node.next.prev = node.prev
                node.prev.next = node.next
            elif i == self.length and i == index:
                node.prev.next = None
                self.tail = node.prev
            node = node.next
        self.length -= 1

    def seekFromHead(self, position):
        """Return key present at given position from head of the linkedlist."""
        if position < 1 or position > self.length:
            return -1
        node = self.head
        for i in range(1, position):
            node = node.next
        return node.key

    def linearSearch(self, value):
        """Return value if value present in the list else None."""
        linkedList = self.head
        while True:
            if linkedList.key == value:
                return value
            elif not linkedList.next:
                return None
            linkedList = linkedList.next


dll = DoubleLinkedList()
dll.append(1)
dll.append(5)
dll.append(6)
dll.append(7)
dll.linearSearch(5)
